package testCreator;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.io.FileNotFoundException;

/*class MethodHeader: refers to a method header within a piece of code*/
public class Method
{
    /* static inner class that is the Pair returned which represents the
     * index of the opening and closing brace of the method*/
    static class Pair
    {
        private int a;
        private int b;
        public Pair(int a, int b)
        {
            this.a = a;
            this.b = b;
        }
        @Override
        public String toString()
        {
            return new String(a + ", " + b);
        }
        /* access methods */
        public int getA()
        {
            return this.a;
        }
        public int getB()
        {
            return this.b;
        }
    }
    /* method attributes */
    private static final int FIND_NAME_OFFSET_METHOD_NAME = 0;
    private static final int FIND_NAME_OFFSET_RETURN_TYPE = -1;
    private static final String METHOD_SIGNATURE_REGEX = "METHOD_SIGNATURE_REGEX";
    public final boolean staticMethod;
    public final boolean constructor;
    private String header; // the method header as it appears in the code.
    private String commentsBetweenHeaderAndBody;
    private String returnType;
    private String methodName;
    private int firstMethodHeaderTokenIndex;
    private int lastMethodHeaderTokenIndex;
    private int leftBraceIndex;
    private int rightBraceIndex;
    private List<Line> lineList;
    private SpecificCodeUtils specificCodeUtils;
    /* constructor. For now we only need to know when the methodSignature starts and ends */
    public Method(String language, String header, int firstMethodHeaderTokenIndex, int lastMethodHeaderTokenIndex, boolean staticMethod, boolean constructor) throws FileNotFoundException
    {
        this.header = header;
        this.firstMethodHeaderTokenIndex = firstMethodHeaderTokenIndex;
        this.lastMethodHeaderTokenIndex = lastMethodHeaderTokenIndex;
        this.staticMethod = staticMethod;
        this.constructor = constructor;
        this.lineList = new ArrayList<Line>();
        this.specificCodeUtils = new SpecificCodeUtils(language);
        this.methodName = findName(FIND_NAME_OFFSET_METHOD_NAME);
        this.returnType = findName(FIND_NAME_OFFSET_RETURN_TYPE);
    }
    public void findNameAndReturnType(boolean findReturnType)
    {
        this.methodName = findName(0);
        String methodReturnType = new String();
        if (!constructor && findReturnType)
        {
            methodReturnType = findReturnType();
        }
        this.returnType = methodReturnType;
    }
    private List<Line> deepCopyLineList(List<Line> lineList)
    {
        // create a deep copy of all the Line objects in lineList
        List<Line> lineListCopy = new ArrayList<Line>();
        for (int i = 0; i < lineList.size(); i++)
        {
            Line toCopy = lineList.get(i);
            Line toAdd = new Line(toCopy.toString(), toCopy.getCharIndexTokenIndexList());
            toAdd.setNumberIndents(toCopy.getNumberIndents());
            lineListCopy.add(toAdd);
        }
        return lineListCopy;
    }
    /* returns the name of the method (method signature excluding brackets) */
    private String findName(int offSet)
    {
        String name = new String();
        StringBuilder methodSignatureSb = new StringBuilder();
        String[] headerStringArray = this.header.split(Character.toString(GenericCodeUtils.SPACE));
        Map<String, String> languageStrings = specificCodeUtils.getLanguageStringsMap();
        String methodSignatureRegex = languageStrings.get(METHOD_SIGNATURE_REGEX);
        // language-specific patterns, tokens
        for (int i = headerStringArray.length - 1; i >= 0; i--)
            // start the last token in the method signature,
            // work backwards, appending all the preceding tokens until a method signature is matched for
            // then extract the name of the method
        {
            methodSignatureSb.insert(0, GenericCodeUtils.SPACE + headerStringArray[i]);
            String testString = methodSignatureSb.toString().trim();
            boolean testStringIsMethodSignature = testString.matches(methodSignatureRegex);
            if (testStringIsMethodSignature)
            {
                if (offSet == 0)
                {
                    name = testString.substring(0, testString.indexOf(GenericCodeUtils.LEFT_BRACKET));
                }
                else
                {
                    if (i + offSet < 0 || i + offSet >= headerStringArray.length)
                    {
                         throw new IllegalArgumentException("offSet out of range!: " + i + offSet);
                    }
                    name = headerStringArray[i + offSet];
                }
            }
        }
        return name;
    }
    /* returns the token in the method header before the method signature */
    private String findReturnType()
    {
        String returnType = new String();
        returnType = findName(-1);
        return returnType;
    }
    /* setter methods */
    public void setHeader(String header)
    {
        this.header = header;
    }
    public void setMethodHeaderIndeces(int firstMethodHeaderTokenIndex, int lastMethodHeaderTokenIndex)
    {
        this.firstMethodHeaderTokenIndex = firstMethodHeaderTokenIndex;
        this.lastMethodHeaderTokenIndex = lastMethodHeaderTokenIndex;
    }
    public void setBraceIndeces(int leftBraceIndex, int rightBraceIndex)
    {
         this.leftBraceIndex = leftBraceIndex;
         this.rightBraceIndex = rightBraceIndex;
    }
    public void setLineList(List<Line> lineList)
        // use a deep copy as the attribute since we want to maintain composition
    {
        List<Line> lineListCopy = deepCopyLineList(lineList);
        this.lineList = lineListCopy;
    }
    public void setCommentsBetweenHeaderAndBody(String comments)
    {
        this.commentsBetweenHeaderAndBody = comments;
    }
    /* access methods */
    public Pair getMethodHeaderIndeces()
    {
         return new Pair(this.firstMethodHeaderTokenIndex, this.lastMethodHeaderTokenIndex);
    }
    public Pair getBraceIndeces()
    {
         return new Pair(this.leftBraceIndex, this.rightBraceIndex);
    }
    public String getHeader()
    {
        return this.header;
    }
    public String getBody()
    {
        StringBuilder methodBody = new StringBuilder();
        for (int i = 0; i < lineList.size(); i++)
        {
            Line thisLine = lineList.get(i);
            for (int indents = 0; indents < thisLine.getNumberIndents(); indents++)
            {
                methodBody.append(GenericCodeUtils.TAB);
            }
            methodBody.append(thisLine.toString());
        }
        return methodBody.toString();
    }
    public List<Line> getLineList()
    {
        List<Line> copy = deepCopyLineList(this.lineList);
        return copy;
    }
    public String getCommentsBetweenHeaderAndBody()
    {
        return this.commentsBetweenHeaderAndBody;
    }
    public String getReturnType()
    {
        return this.returnType;
    }
    public String getName()
    {
        return this.methodName;
    }
    /* toString method*/
    @Override
    public String toString()
    {
        return getHeader() + "\n" + getCommentsBetweenHeaderAndBody() + getBody();
    }
}
