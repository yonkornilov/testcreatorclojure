String toString()
{
	return new String(a + ", " + b);
}
int getA()
{
	return this.a;
}
int getB()
{
	return this.b;
}
void findNameAndReturnType(boolean findReturnType)
{
	this.methodName = findName(0);
	String methodReturnType = new String();
	if(! constructor && findReturnType)
	{
		methodReturnType = findReturnType();
	}
	this.returnType = methodReturnType;
}
List < Line > deepCopyLineList(List < Line > lineList)
{
	List < Line > lineListCopy = new ArrayList < Line >();
	for(int i = 0;
	i < lineList.size();
	i ++)
	{
		Line toCopy = lineList.get(i);
		Line toAdd = new Line(toCopy.toString(), toCopy.getCharIndexTokenIndexList());
		toAdd.setNumberIndents(toCopy.getNumberIndents());
		lineListCopy.add(toAdd);
	}
	return lineListCopy;
}
String findName(int offSet)
{
	String name = new String();
	StringBuilder methodSignatureSb = new StringBuilder();
	String[]headerStringArray = this.header.split(Character.toString(GenericCodeUtils.SPACE));
	Map < String, String > languageStrings = specificCodeUtils.getLanguageStringsMap();
	String methodSignatureRegex = languageStrings.get(METHOD_SIGNATURE_REGEX);
	for(int i = headerStringArray.length - 1;
	i >= 0;
	i --)
	{
		methodSignatureSb.insert(0, GenericCodeUtils.SPACE + headerStringArray[i]);
		String testString = methodSignatureSb.toString().trim();
		boolean testStringIsMethodSignature = testString.matches(methodSignatureRegex);
		if(testStringIsMethodSignature)
		{
			if(offSet == 0)
			{
				name = testString.substring(0, testString.indexOf(GenericCodeUtils.LEFT_BRACKET));
			}
			else
			{
				if(i + offSet < 0 || i + offSet >= headerStringArray.length)
				{
					throw new IllegalArgumentException("offSet out of range!: " + i + offSet);
				}
				name = headerStringArray[i + offSet];
			}
		}
	}
	return name;
}
String findReturnType()
{
	String returnType = new String();
	returnType = findName(- 1);
	return returnType;
}
void setHeader(String header)
{
	this.header = header;
}
void setMethodHeaderIndeces(int firstMethodHeaderTokenIndex, int lastMethodHeaderTokenIndex)
{
	this.firstMethodHeaderTokenIndex = firstMethodHeaderTokenIndex;
	this.lastMethodHeaderTokenIndex = lastMethodHeaderTokenIndex;
}
void setBraceIndeces(int leftBraceIndex, int rightBraceIndex)
{
	this.leftBraceIndex = leftBraceIndex;
	this.rightBraceIndex = rightBraceIndex;
}
void setLineList(List < Line > lineList)
{
	List < Line > lineListCopy = deepCopyLineList(lineList);
	this.lineList = lineListCopy;
}
void setCommentsBetweenHeaderAndBody(String comments)
{
	this.commentsBetweenHeaderAndBody = comments;
}
Pair getMethodHeaderIndeces()
{
	return new Pair(this.firstMethodHeaderTokenIndex, this.lastMethodHeaderTokenIndex);
}
Pair getBraceIndeces()
{
	return new Pair(this.leftBraceIndex, this.rightBraceIndex);
}
String getHeader()
{
	return this.header;
}
String getBody()
{
	StringBuilder methodBody = new StringBuilder();
	for(int i = 0;
	i < lineList.size();
	i ++)
	{
		Line thisLine = lineList.get(i);
		for(int indents = 0;
		indents < thisLine.getNumberIndents();
		indents ++)
		{
			methodBody.append(GenericCodeUtils.TAB);
		}
		methodBody.append(thisLine.toString());
	}
	return methodBody.toString();
}
List < Line > getLineList()
{
	List < Line > copy = deepCopyLineList(this.lineList);
	return copy;
}
String getCommentsBetweenHeaderAndBody()
{
	return this.commentsBetweenHeaderAndBody;
}
String getReturnType()
{
	return this.returnType;
}
String getName()
{
	return this.methodName;
}
String toString()
{
	return getHeader()+ "\n" + getCommentsBetweenHeaderAndBody()+ getBody();
}
