public class ArrayTest
{
    static float[] floatTestCases = {-1.4E-45f, -1.4E-4f, -1.4E-1f, 1.4E-45f, 1.4E-4f, 1.4E-1f, 0.0f, -1.4f, -1.4E4f, -3.4028235E3f, -3.4028235E38f, 1.4f, 1.4E4f, 3.4028235E3f, 3.4028235E38f};
    public static void main(String[] args)
    {
        for (int i = 0; i < floatTestCases.length; i++)
        {
            System.out.println(floatTestCases[i]);
        }
    }
}
