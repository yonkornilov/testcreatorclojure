(ns test-creator.parser
  (:require [clj-antlr.core :as antlr]
            [clojure.zip :as z]))

(def c-sharp-parser (antlr/parser "resources/csharp/CSharp4.g4" {:root "compilation_unit"}))

(def java-parser (antlr/parser "resources/java/Java.g4"))

(def java-language-identifiers
  {:class-body :classBody
   :class-declaration :classDeclaration
   :method-declaration :methodDeclaration
   :type :type
   :var-id :variableDeclaratorId
   :parameter-list :formalParameterList
   :parameter :formalParameter
   :primitive-type :primitiveType
   :object-type :classOrInterfaceType})

(defn list-tree-zipper
  [tree]
  (z/zipper seq? rest conj tree))

(defn find-methods
  [tree language-identifiers]
  (let [tree-zipper
        (list-tree-zipper tree)]
    ((fn
       [zipper zipper-set]
       (if (contains? zipper-set zipper)
         (filter #(= (language-identifiers :method-declaration) (first (z/node %))) zipper-set)
         (recur (z/next zipper) (conj zipper-set zipper)))) tree-zipper #{})))
