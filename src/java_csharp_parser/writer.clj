(ns test-creator.writer
  (:require [clojure.zip :as z])
  (:use test-creator.parser))

(defn tree-get-tokens
  [tree]
  (drop-last (filter string? (flatten tree))))

(def java-token-style-rules
  ;; token [prefix - suffix - trim-right-whitespace? & exception-token-input-function]
  {";" ["" \newline true]
   "(" ["" "" true]
   ")" ["" "" true]
   "." ["" "" true]
   "," ["" " " true]
   "{" [\newline \newline true (fn [tokens] (= "=" (last tokens)))]
   "}" ["" \newline false]
   "@" ["" "" false]
   "[" ["" "" true]
   "]" ["" " " true]
   })

(defn style-tokens
  [input style-rules]
  (loop [input input
         output ""]
    (if (empty? input)
      output
      (let [token  (first input)
            input' (rest input)
            token-style (style-rules token)
            token (if token-style
                    (str (first token-style) token (second token-style))
                    (str token " "))
            output (if (nth token-style 2)
                     (str (clojure.string/trimr output) token)
                     (str output token))]
        (recur (rest input) output)))))

;; line [indent-level-change add-indents-current-line?]
(def line-indents
  {"{"  [1  false]
   "}"  [-1 true]})

(defn indent-lines
  "Given a string, indent each line based on the previous lines and
  the indentation rules defined in the namespace."
  [input]
  (loop [lines (clojure.string/split-lines input)
        indent-level 0 ;; how many tabs for the current line
        output ""]
    (if (empty? lines)
      output
      (let [current-line (first lines)
            get-indents #(apply str (into '[] (repeat % \tab)))
            old-indents (get-indents indent-level)
            indent-level-change (line-indents current-line)
            indent-level (if indent-level-change
                           (+ indent-level (first indent-level-change))
                           indent-level)
            new-indents (get-indents indent-level)
            current-line (if (second indent-level-change)
                           (str new-indents current-line \newline)
                           (str old-indents current-line \newline))]
        (recur (rest lines) indent-level (str output current-line))))))
    
(defn parse-input
  [tokens style-rules]
  (indent-lines
   (style-tokens tokens style-rules)))

(def java-type-casts
  {"long"  ["" "L"]
   "char"  ["(char)" ""]
   "byte"  ["(byte)" ""]
   "float" ["" "f"]})

(def test-array-name-post-fix "TestCases")

(defn test-array-name
  [type]
  (str type test-array-name-post-fix))

(defn create-test-array
  [type type-casts & prefix-attribute]
  (if-let [values (seq (take-while #(not= \: (last %))
                  (rest (drop-while #(not= (str type ":") %)
                                    (clojure.string/split-lines
                                     (slurp "resources/type-test-case-values.txt"))))))]
    (let [cast (type-casts type)]
      (str
      (first prefix-attribute) type "[] " (test-array-name type) " = {"
      (apply str (map #(str (first cast) % (second cast) ", ") (drop-last values)))
      (first cast) (last values) (last cast) "}"))))

(def java-for-loop-tokens
  ["for" "(" :index-var-type :index-var-id "=" "0" ";"
   :index-var-id "<" :array-length-get-prefix :array-name :array-length-get-postfix ";"
   :index-var-id "++" ")"
   "{"
   :body
   "}"])

(def java-for-loop-smap
  {:index-var-type "int"
   :array-length-get-postfix ["." "length"]})

(defn create-for-loop
  [type body nested-level for-loop-tokens for-loop-smap]
  (let [updated-smap (merge
                      for-loop-smap
                      {:index-var-id (str "level" nested-level "Idx")
                       :array-name (test-array-name type)
                       :body body})]
  (flatten (replace updated-smap for-loop-tokens))))
