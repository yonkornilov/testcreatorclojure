(ns test-creator.core
  (:gen-class)
  (:require [clojure.zip :as z])
  (:use test-creator.writer test-creator.parser))

(def java-tree (let [tree (java-parser
                           (slurp "resources/ToTest.java"))] tree))

(def java-tree-zipper (list-tree-zipper java-tree))

(defn output-tester
  "Given a zipper, returns a string of its tokens, formatted and indented
  with the given style rules."
  [zipper token-style-rules]
  (parse-input (tree-get-tokens (z/root zipper)) token-style-rules))

(defn all-sub-zippers
  "Returns all zippers, in the same order as clojure.zip/next, whose nodes
  branch from the node at the loc of the given zipper."
  [zipper]
  (let [parent-path-count (count (z/path zipper))]
    (letfn [(zipper-cycler
              [current-zipper acc]
              (if (and (<= (count (z/path current-zipper)) parent-path-count)
                       (not= zipper current-zipper))
               acc
               (recur (z/next current-zipper) (conj acc current-zipper))))]
      (zipper-cycler zipper []))))

(defn filter-branches-keywords
  "Returns a seq of zippers whose nodes are a method declaration."
  [zipper pred]
  (filter #(and (seq? %) (pred (first %)))
                 (map z/node (all-sub-zippers zipper))))

;; temporary
(defn first-method-zipper
  [zipper]
  (letfn [(find-first-method
            [current-zipper]
            (if (= :methodDeclaration (first (z/node current-zipper)))
              current-zipper
              (recur (z/next current-zipper))))]
    (find-first-method zipper)))

(defn find-method-param-types
  "Given a zipper whose node at its current loc is a method declaration,
  returns a seq of lists containing the lexer's identifying keyword for this sort
  of type, and the actual name of the type (but not the declarator id)."
  [method-zipper language-identifiers]
  (map second
       (->
        method-zipper
        (filter-branches-keywords #{(language-identifiers :type)})
        distinct)))

(defn update-methods
  [zipper]
  (letfn [(method-updater [current-zipper]
            (if (= current-zipper zipper)
              current-zipper
              (recur (z/next current-zipper))))]
    (method-updater zipper)))
